# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: C:\rtlaudiolab\blog\038_hello_world_vitis\vitis\hello_world_vitis_system\_ide\scripts\debugger_hello_world_vitis_app-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source C:\rtlaudiolab\blog\038_hello_world_vitis\vitis\hello_world_vitis_system\_ide\scripts\debugger_hello_world_vitis_app-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zed 210248518579" && level==0 && jtag_device_ctx=="jsn-Zed-210248518579-23727093-0"}
fpga -file C:/rtlaudiolab/blog/038_hello_world_vitis/vitis/hello_world_vitis_app/_ide/bitstream/hello_world_vitis.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw C:/rtlaudiolab/blog/038_hello_world_vitis/vitis/hello_world_vitis_hw/export/hello_world_vitis_hw/hw/hello_world_vitis.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source C:/rtlaudiolab/blog/038_hello_world_vitis/vitis/hello_world_vitis_app/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow C:/rtlaudiolab/blog/038_hello_world_vitis/vitis/hello_world_vitis_app/Debug/hello_world_vitis_app.elf
configparams force-mem-access 0
bpadd -addr &main
